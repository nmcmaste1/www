let k, hero, sword
class Main extends Phaser.Scene {
    preload(){
        this.load.image('hero', "assets/hero.png")
        this.load.image('sword', "assets/sword.png")
    }
    create(){
        hero = this.add.image(200, 200, 'hero').setScale(3)
        this.add.image(hero.x-15, hero.y-12, 'sword').setScale(3)
        k = this.input.keyboard.addKeys('W,A,S,D,UP,RIGHT,LEFT,DOWN,SPACE,ENTER,R')
    }
    update(){
        let SPACE = k.SPACE.isDown
        let ENTER = k.ENTER.isDown
        let up = k.UP.isDown
        let down = k.DOWN.isDown
        let right = k.RIGHT.isDown
        let left = k.LEFT.isDown
        let W = k.W.isDown
        let S = k.S.isDown
        let D = k.D.isDown
        let A = k.A.isDown
    
    }
}
const game = new Phaser.Game({
    scene: Main, height:768, width:1024,
    scale: Phaser.Scale.FIT,
   autoCenter: Phaser.Scale.CENTER_BOTH,
   pixelArt:true
  })