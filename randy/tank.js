let k, tank
let stairs
class Main extends Phaser.Scene {

  preload() {
    this.load.image('tank', "assets/tank.png")
    this.load.image('bgf', "assets/bgf.png")
    this.load.image('stairs', "assets/7.png")
    this.load.image('laser', "assets/white_square.png")
    this.load.image('phaser', "assets/white_square.png")
    this.load.spritesheet('tank-clone',
      'assets/tank-clone.png',
      { frameWidth: 57, frameHeight: 56 });
  }

  create() {
    this.add.image(0, 0, 'bgf').setOrigin(0, 0)
    this.anims.create({
      key: 'explode',
      frames: this.anims.generateFrameNumbers('tank-clone', { start: 0, end: 4 }),
      frameRate: 10,
      repeat: 0
    })

    stairs = this.add.image(200, 200, 'stairs') 
    stairs.speed = 11
    tank = this.add.image(923, 658, 'tank')
    tank.speed = 11
    tank.rotation = Math.PI
    k = this.input.keyboard.addKeys('W,A,S,D,UP,RIGHT,LEFT,DOWN,SPACE,ENTER,R')
  }

  death(p) {
    p.destroy()
    p.dead = true
    let clone = this.add.sprite(p.x, p.y, 'tank-clone')
    clone.play('explode')
    setTimeout(() => {
      clone.destroy()
    }, 900)
  }

  update() {
    let SPACE = k.SPACE.isDown
    let ENTER = k.ENTER.isDown
    let up = k.UP.isDown
    let down = k.DOWN.isDown
    let right = k.RIGHT.isDown
    let left = k.LEFT.isDown
    let W = k.W.isDown
    let S = k.S.isDown
    let D = k.D.isDown
    let A = k.A.isDown

    tank.rect = tank.getBounds()
    stairs.rect = stairs.getBounds()

    if (k.R.isDown) {
      this.scene.restart()
      stairs.dead = false
      tank.dead = false
    }

    if (!tank.dead) {
      if (up) {
        tank.y -= tank.speed
        tank.rotation = Math.PI / -2
      }
      if (down) {
        tank.y += tank.speed
        tank.rotation = Math.PI / 2
      }
      if (right) {
        tank.x += tank.speed
        tank.rotation = 0
      }
      if (left) {
        tank.x -= tank.speed
        tank.rotation = Math.PI
      }
      if (left && up) {
        tank.rotation = Math.PI / -1.5
      }
      if (left && down) {
        tank.rotation = Math.PI / 1.5
      }
      if (right && down) {
        tank.rotation = Math.PI / 4
      }
      if (right && up) {
        tank.rotation = Math.PI / -4
      }

      if (ENTER) {
        let line = new Phaser.Geom.Line(tank.x, tank.y, 100, 100)
        Phaser.Geom.Line.SetToAngle(line, tank.x, tank.y, tank.rotation, 1200)
        let graphics = this.add.graphics()
        graphics.lineStyle(0.5, 0xffffff)
        graphics.strokeLineShape(line)
        //graphics.strokeRectShape(stairs.rect)
        if (Phaser.Geom.Intersects.LineToRectangle(line, stairs.rect)) {
          this.death(stairs)
        }
        setTimeout(() => {
          graphics.clear()
        }, 500)
      }
    }

    if (!stairs.dead) {
      if (SPACE) {
        let line = new Phaser.Geom.Line(stairs.x, stairs.y, 100, 100)
        Phaser.Geom.Line.SetToAngle(line, stairs.x, stairs.y, stairs.rotation, 1200)
        let graphics = this.add.graphics()
        graphics.lineStyle(0.5, 0x0000ff)
        graphics.strokeLineShape(line)
        //graphics.strokeRectShape(tank.rect)
        if (Phaser.Geom.Intersects.LineToRectangle(line, tank.rect)) {
          this.death(tank)
        }
        setTimeout(() => {
          graphics.clear()
        }, 500)
      }

      if (W) {
        stairs.y -= stairs.speed
        stairs.rotation = Math.PI / -2
      }
      if (S) {
        stairs.y += stairs.speed
        stairs.rotation = Math.PI / 2
      }
      if (D) {
        stairs.x += stairs.speed
        stairs.rotation = 0
      }
      if (A) {
        stairs.x -= stairs.speed
        stairs.rotation = Math.PI
      }
      if (A && W) {
        stairs.rotation = Math.PI / -1.5
      }
      if (A && S) {
        stairs.rotation = Math.PI / 1.5
      }
      if (D && S) {
        stairs.rotation = Math.PI / 4
      }
      if (D && W) {
        stairs.rotation = Math.PI / -4
      }
    }
    if (stairs.x<-20) stairs.x = -20
    if (stairs.x>1044) stairs.x = 1044 
    if (stairs.y<-20) stairs.y = -20
    if (stairs.y>788) stairs.y = 788 
    if (tank.x<-20) tank.x = -20
    if (tank.x>1044) tank.x = 1044 
    if (tank.y<-20) tank.y = -20
    if (tank.y>788) tank.y = 788 

  }
}

const game = new Phaser.Game({
  scene: Main, height:768, width:1024,
  scale: Phaser.Scale.FIT,
 autoCenter: Phaser.Scale.CENTER_BOTH,
})


